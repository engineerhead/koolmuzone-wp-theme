<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width,initial-scale=1.0">

	<link rel="icon" type="image/png" href="img/favicon.ico">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>?ver=1.0">
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/KM-responsive.css"  />
	
	<title> <?php wp_title(); ?> </title>
</head>
<body <?php body_class(); ?>>
	<div id="fb-root"></div>

	<?php do_action( 'before' ); ?>
		<header id="masthead" class="site-header" role="banner">
			<div class="header">
				<div class="muzone"></div>
				<div class="kool"></div>
				<div class="container">
					<div class="logo_holder">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php bloginfo('template_directory'); ?>/img/logo.png" width="136" height="71" /></a>
					</div>
					 <div class="top_add">
					 	<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Ad in Header')) : else : ?>
					 		<h2>Sidebar</h2>
					 	<?php endif; ?>
					 </div>
				</div>
			</div>

			<?php get_template_part( 'nav', 'tags'); ?>
			<?php get_template_part( 'nav', 'categories'); ?>

		</header><!-- #masthead -->

		<div class="outer">
			<div class="container">
				<div class="content">