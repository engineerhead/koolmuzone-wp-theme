<?php

// Cleans up Head
function removeHeadLinks() 
{
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wlwmanifest_link');
}
add_action('init', 'removeHeadLinks');
remove_action('wp_head', 'wp_generator');

// Enable shortcode in Widgets
add_filter('widget_text', 'do_shortcode');


// Enable Thumbnails
add_theme_support( 'post-thumbnails' );

// Add Scripts Scripts 
add_action('wp_enqueue_scripts','km_scripts');
function km_scripts()
{
    wp_register_script('holictheme',get_template_directory_uri().'/js/main.js',array('jquery'),'1',1);
    wp_enqueue_script('holictheme');
}

//Excluding Categories
function exclude_category( $query ) {
    if ( $query->is_home() && $query->is_main_query() ) {
        $query->set( 'cat', '-8,-33,-94');
    }
}

add_action( 'pre_get_posts', 'exclude_category' );

// Register Menu
function register_km_menu() {
  register_nav_menus( array(
      'categories' => __( 'Categories Menu', 'kool-muzone' ),
      'tags' => __( 'Tags Menu', 'kool-muzone' ),
  ));
}
add_action( 'init', 'register_km_menu' );


// Register Sidebars
function register_km_sidebars()

{
    //Sidebar

    register_sidebar(array(

                    'name' => __('Sidebar'),

                    'id'   => 'sidebar-widgets',

                    'description'   => __( 'These are widgets for the Sidebar.'),

                    'before_widget' => '<div id="%1$s" class="widget %2$s">',

                    'after_widget'  => '</div>',

                    'before_title'  => '<div class="w_heading"><h3 style="display: inline;">',

                    'after_title'   => '</h3></div>'

            ));

    

    //Header Ad Widget

    register_sidebar(array(

                    'name' => __('Ad in Header'),

                    'id'   => 'ad-in-header',

                    'description'   => __( 'This is widget for the Ad in Header'),

                    'before_widget' => '<div id="%1$s">',

                    'after_widget'  => '</div>',

                    'before_title'  => '<h3>',

                    'after_title'   => '</h3>'

            ));      

}
add_action('widgets_init','register_km_sidebars');


// Show time in Human Readable Format unless not Old than A week
function dynamictime() {

  global $post;

  $date = $post->post_date;

  $time = get_post_time('G', true, $post);

  $mytime = time() - $time;

  if($mytime > 0 && $mytime < 7*24*60*60)

    $mytimestamp = sprintf(__('%s ago'), human_time_diff($time));

  else

    $mytimestamp = date(get_option('date_format'), strtotime($date));

  return $mytimestamp;

}
add_filter('the_time', 'dynamictime');

// Post Meta
 function km_post_meta()
{
?>
<div class="info">
    <div class="date"><?php the_time(); ?> </div>
    <div class="author"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author_meta( 'display_name' ); ?></a></div>
    <div class="tag"><?php the_category(', ') ?></div>
</div>
<?php
  if(is_user_logged_in()){
    edit_post_link();
  }
}


/**
 * Social Buttons 
 */
function km_social_bar()
{
  $link = get_permalink();
  $title = get_the_title();
?>
<div class="social-bar">
  <div class="pw-widget"  pw:url="<?php echo $link; ?>" pw:title="<?php echo $title; ?>">
    <a class="pw-button-facebook pw-look-native"></a>
    <a class="pw-button-twitter pw-look-native"></a>
    <a class="pw-button-linkedin pw-look-native"></a>
    <a class="pw-button-googleplus pw-look-native"></a>
  </div> 
</div>

<?php

}


function get_fbimage() {
  $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '', '' );
  if ( has_post_thumbnail($post->ID) ) {
    $fbimage = $src[0];
  } else {
    global $post, $posts;
    $fbimage = '';
    $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i',
    $post->post_content, $matches);
    $fbimage = $matches [1] [0];
  }
  if(empty($fbimage)) {
    $fbimage = "http://mydomain.com/default-image.jpg";
  }
  return $fbimage;
}

function km_pagination(){

    global $wp_query;

    $big = 999999999; // need an unlikely integer

    echo '<div class="pagination">';
    echo paginate_links( array(
      'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
      'format' => '?paged=%#%',
      'current' => max( 1, get_query_var('paged') ),
      'total' => $wp_query->max_num_pages,
      'type' => 'list'
    ) );
    echo '</div>';

}


?>