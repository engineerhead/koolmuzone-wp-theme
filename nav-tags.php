<div class="navbar hashtags" id="hashbar">
	<div class="container">
 		 <div class="navbar-inner">
   			 <?php wp_nav_menu( array( 'theme_location' => 'tags', 'container' => 'nav', 'menu_class' => 'nav' ) );?>
             <form class="navbar-form pull-right">
              <div class="input-append">
              <input class="span3" style="height:15px;" id="appendedInputButton" type="text" placeholder="Search">
              <button class="btn btn-danger" type="button">
              	<img src="<?php bloginfo('template_directory'); ?>/img/icon_search_3.png" width="15" height="15" /></button>
            </div>
            </form>
  		</div><!-- navbar inner end -->
  </div><!-- container end -->
</div> <!-- hashtags barend -->