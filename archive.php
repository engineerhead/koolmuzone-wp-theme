<?php
get_header();
?>


<div id="blog">

	<?php 
		if (have_posts()) :  
			while (have_posts()): 
				the_post();  
	?>
		<article>
	      <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
					
				<?php km_post_meta(); ?>
				
				<div class="f_content">
	       		<?php if(has_post_thumbnail) the_post_thumbnail('thumbnail', array('class' => 'w_hover'));?>	
				</div>

				      						
				<div class="blog_content">
					<?php the_excerpt(); ?>
					<div class="read_more"><a href="<?php the_permalink(); ?>" class="general_read_more">Read More</a></div>
				</div>
				
				<div class="clearboth"></div>
		</article>		<!-- article end -->	

	<?php
		endwhile;
		endif;

		km_pagination();
	?>

</div> <!-- /blog -->
			
<?php
	get_sidebar();
	get_footer();
?>