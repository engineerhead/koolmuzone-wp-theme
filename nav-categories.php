<div class="row-fluid"> <!-- navigation menu -->
  <div class="navbar menu_nav">
    <div class="container">
     <div class="navbar-inner">
       <div class="container-fluid">
          <div class="menutext">Menu</div>
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">    
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
            </a> 

    <div class="nav-collapse collapse">
      <?php wp_nav_menu( array( 'theme_location' => 'categories', 'container' => 'nav', 'menu_class' => 'nav' ) );?>
    </div><!-- /.nav-collapse -->
    </div><!-- /.container -->
  </div><!-- /.navbar-inner -->
  </div> <!-- /.container -->
</div><!-- /.navbar -->