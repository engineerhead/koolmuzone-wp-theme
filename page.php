<?php
get_header();
?>


<div id="blog">

	<?php 
		if (have_posts()) :  
			while (have_posts()): 
				the_post();  
	?>
	<article>
      <h3><?php the_title(); ?></h3>
			
			<?php the_content(); ?>
	</article>		<!-- article end -->

	<?php
		endwhile;
		endif;
	?>

</div> <!-- /blog -->
			
<?php
	get_sidebar();
	get_footer();
?>