<?php
	global $section;
	if( $section == 'Download' ):
		$cat = '94';	
?>
<article class="download">
	<div class="heading">
		<h3 style="display:inline" class="custom_heading">Downloads</h3>
		<div class="down_urdu"></div>
	</div>

<?php
	else:
		$cat = '8,33';
?>
<article class="download video">
	<div class="heading">
		<h3 style="display:inline">Videos & Pictures</h3>
		<div class="video_urdu"></div>
	</div>
	<div class="duck"></div>
<?php
	endif;
?>

	<div class="duck"></div>
	<div class="download_container">
		<div class="row-fluid offset0">
  	        
  	        	<?php
  	        	$args = array( 'posts_per_page' => 6, 'category' => $cat );
  	        	$i = 1;
  	        	$myposts = get_posts( $args );
  	        	foreach ( $myposts as $post ) : setup_postdata( $post ); 
  	        		if( $i == 1 || $i == 4 )
  	        		{
  	        			echo "<ul class='thumbnails'>";
  	        		}
  	        	?>
    	        		<li class="span3">
    	        		  <a href="<?php the_permalink();?>" class="thumbnail">
    	        		  	<?php
    	        		  		if( has_post_thumbnail())
    	        		  		{
    	        		  			the_post_thumbnail('thumbnail');
    	        		  		}
    	        		  	?>
    	        		    	
    	        			</a>
    	        			<span><?php the_title(); ?></span>
    	        		</li>
  	        	<?php
  	        		if( $i == 3 || $i == 6 )
  	        		{
  	        			echo "</ul>";
  	        		} 
  	        		$i++;
  	        		endforeach; 
  	        		wp_reset_postdata();
  	        		?>
      </div>
  </div>
</article>