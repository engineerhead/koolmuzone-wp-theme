<?php
get_header();
?>


<div id="blog">

	<?php
		$section = 'Download';
		get_template_part('section'); 
		if (have_posts()) :
			$j = 0;  
			while (have_posts()): 
				$j++;
				the_post();  
	?>
	<article>
      <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
				
			<?php km_post_meta(); ?>
			
			<div class="f_content">
       		<?php if(has_post_thumbnail) the_post_thumbnail('thumbnail', array('class' => 'w_hover'));?>	
			</div>

			      						
			<div class="blog_content">
				<?php the_excerpt(); ?>
				<div class="read_more"><a href="<?php the_permalink(); ?>" class="general_read_more">Read More</a></div>
			</div>
			
			<div class="clearboth"></div>
	</article>		<!-- article end -->	

	<?php
		if($j == 2){
			$section = 'Video';
			get_template_part( 'section');
		}
		endwhile;
		endif;

		km_pagination();
	?>

</div> <!-- /blog -->
			
<?php
	get_sidebar();
	get_footer();
?>