<?php
get_header();
?>


<div id="blog">

	<?php 
		if (have_posts()) :  
			while (have_posts()): 
				the_post();  
	?>
	<article>
      <h4><?php the_title(); ?></h4>
				
			<?php km_post_meta(); ?>
			
			<?php the_content(); ?>
	</article>		<!-- article end -->

	<?php
		endwhile;
		endif;
	?>

</div> <!-- /blog -->
			
<?php
	get_sidebar();
	get_footer();
?>